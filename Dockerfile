FROM composer:1.8 as composer-container

FROM php:7.3-apache-stretch

# fix postgre install, see https://github.com/debuerreotype/debuerreotype/issues/10#issuecomment-450480318
RUN seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{}
RUN apt-get update -y
RUN apt-get install --no-install-recommends -y \
    git \
    libpng-dev \
    libpq-dev \
    postgresql \
    postgresql-contrib
RUN docker-php-ext-install \
    gd \
    opcache \
    mysqli \
    pdo \
    pdo_pgsql
RUN docker-php-ext-enable opcache
RUN rm -rf /var/lib/apt/lists/*
COPY ./provisioning/php.ini "/tmp/php.ini"
RUN mv "/tmp/php.ini" "$PHP_INI_DIR/php.ini"
COPY ./provisioning/apache-vh.conf /etc/apache2/sites-enabled/000-default.conf
COPY ./provisioning/opcache.ini $PHP_INI_DIR/conf.d/

COPY --from=composer-container /usr/bin/composer /usr/bin/composer
COPY . /var/www/html
WORKDIR /var/www/html
RUN composer global require hirak/prestissimo --no-plugins --no-scripts
# Running composer twice as post-install-cmd is buggy for now.
RUN composer install --no-dev --no-interaction --no-progress || \
    composer install --no-dev --no-interaction --no-progress
RUN chmod 555 /var/www/html/web/sites/default
RUN chmod 555 /var/www/html/web/sites/default/settings.php
RUN chmod a+x /var/www/html/scripts/*.sh

EXPOSE 5000
