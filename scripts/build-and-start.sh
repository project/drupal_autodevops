#!/bin/sh

BASEDIR="$( cd "$(dirname "$0")" ; pwd -P )"

# Running composer twice as post-install-cmd is buggy for now.
docker run --rm --interactive --tty --volume ${BASEDIR}/..:/app composer install --ignore-platform-reqs || \
  docker run --rm --interactive --tty --volume ${BASEDIR}/..:/app composer install --ignore-platform-reqs
docker-compose -f ${BASEDIR}/../docker-compose.yml build
docker-compose -f ${BASEDIR}/../docker-compose.yml up -d
docker exec -it autodevopsdrupal_web_1 chmod 555 /var/www/html/web/sites/default/settings.php
docker exec -it autodevopsdrupal_web_1 bash /var/www/html/scripts/db-initialize.sh
